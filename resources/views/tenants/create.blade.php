<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add new project') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('tenants.store') }}">
                    @csrf

                    <!-- Name -->
                        <div>
                            <x-input-label for="name" :value="__('Name')"/>

                            <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')"
                                     required autofocus/>
                            <x-input-error :messages="$errors->get('name')" class="mt-2" />

                            <x-input-label for="email" :value="__('Email')"/>

                            <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')"
                                     required autofocus/>
                            <x-input-error :messages="$errors->get('email')" class="mt-2" />

                            <x-input-label for="domain_name" :value="__('Domain Name')"/>

                            <x-text-input id="domain_name" class="block mt-1 w-full" type="text" name="domain_name" :value="old('domain_name')"
                                     required autofocus/>
                            <x-input-error :messages="$errors->get('domain_name')" class="mt-2" />

                            <x-input-label for="password" :value="__('Password')"/>

                            <x-text-input id="password" class="block mt-1 w-full" type="text" name="password" :value="old('password')"
                                     required autofocus/>
                            <x-input-error :messages="$errors->get('password')" class="mt-2" />
                        </div>

                        <div class="flex mt-4">
                            <x-button>
                                {{ __('Save Tenant') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
