<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();
        Role::create(['name' => 'super-admin']);
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'user']);
        Role::create(['name' => 'manager']);

   $user = \App\Models\User::factory()->create([
            'name' => 'Sper Admin',
            'email' => 'super_admin@app.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('super-admin');


    }
}
